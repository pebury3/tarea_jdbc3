package Modulo;

public class Poderes {

    private String nombre_poder;
    private String tipo;
    private int dano;

    public Poderes(String nombre_poderes, String tipo, int dano) {
        this.nombre_poder = nombre_poderes;
        this.tipo = tipo;
        this.dano = dano;
    }

    public String getNombre_poderes() {
        return nombre_poder;
    }

    public void setNombre_poderes(String nombre_poderes) {
        this.nombre_poder = nombre_poderes;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getDaño() {
        return dano;
    }

    public void setDano(int dao) {
        this.dano = dano;
    }

    @Override
    public String toString() {
        return "Poderes{" +
                "nombre_poderes='" + nombre_poder + '\'' +
                ", tipo='" + tipo + '\'' +
                ", daño=" + dano +
                '}';
    }
}
