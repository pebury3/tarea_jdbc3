package Modulo;

import java.util.ArrayList;

public interface JugadorDAO{
    ArrayList <Jugador> select_all0();
    Jugador select_by_id (int id);
    Jugador select_by_nickname(String nickname);
    Jugador select_by_name(String name);
    Jugador select_email(String email);
    Jugador select_clave(int clave);
    boolean insert_jugador(Jugador jugador);
    boolean update_jugador(int id,int id2,String name, String email, int clave, String nickname);
    boolean update_nickname(int id, String nickname);
    boolean delete_jugador_by_id(int id);
}
/*
*  ArrayList<User> select_all();
    User select_by_id(int id);
    double select_avg_salary();
    boolean insert_user(User user);
    boolean delete_user_by_id(int id);
    boolean update_salary_by_id(int id, double salary);
*  */