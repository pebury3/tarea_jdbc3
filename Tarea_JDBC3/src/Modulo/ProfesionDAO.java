package Modulo;

import java.util.ArrayList;

public interface ProfesionDAO {

    ArrayList <Profesion> select_all2();
    Profesion select_tipo_profesion(String tipo_profesion);
    Profesion select_elemento(String elemento);
     Profesion select_vida(int vida);
    int select_avg_vida();
    boolean insert_profesion(Profesion profesion);
    boolean delete_profesion_by_tipo_profesion(String tipo_profesion);
    boolean update_vida_by_tipo_profesion(String tipo_profesion, int vida );
}
