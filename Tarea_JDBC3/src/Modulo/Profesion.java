package Modulo;

public class Profesion {

    private String tipo_profesion;
    private String elemento;
    private int vida;

    public Profesion(String tipo_profesion, String elemento, int vida) {
        this.tipo_profesion = tipo_profesion;
        this.elemento = elemento;
        this.vida = vida;
    }

    public String getTipo_profesion() {
        return tipo_profesion;
    }

    public void setTipo_profesion(String tipo_profesion) {
        this.tipo_profesion = tipo_profesion;
    }

    public String getElemento() {
        return elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    @Override
    public String toString() {
        return "Profesion{" +
                "tipo_profesion='" + tipo_profesion + '\'' +
                ", elemento='" + elemento + '\'' +
                ", vida=" + vida +
                '}';
    }
}
