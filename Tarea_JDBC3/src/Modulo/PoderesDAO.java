package Modulo;

import java.util.ArrayList;

public interface PoderesDAO {
    ArrayList<Poderes> select_all();

    Poderes select_nombre_poder(String nombre_poder);

    Poderes select_tipo(String tipo);
   Poderes select_dano (int dano);
    boolean insert_poderes(Poderes poderes);
    boolean delete_poderes_by_nombre_poder(String nombre_poder);
    boolean update_dano_by_nombre_poder( String nombre_poder, int dano);
    int select_avg_dano();

}
