package Modulo;

public class Jugador {

    private int id;
    private String name;
    private  String email;
    private int clave;
    private String nickname;

    public Jugador(int id, String name, String email, int clave, String nickname) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.clave = clave;
        this.nickname = nickname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "Jugador{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", clave=" + clave +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
