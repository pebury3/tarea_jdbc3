package Controlador;

import Modulo.Jugador;
import Modulo.Poderes;
import Modulo.PoderesDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PoderesJDBC implements PoderesDAO {

    public final String SELECT_ALL = "SELECT * FROM poderes";
    public final String SELECT_NOMBRE_PODER= "SELECT * FROM poderes WHERE nombre_poder = ? ";
    public final String SELECT_TIPO= "SELECT * FROM poderes WHERE tipo = ?";
    public final String SELECT_DANO= "SELECT * FROM poderes WHERE dano = ?";
    public final String INSERT_PODERES = "INSERT poderes VALUES(?, ?, ?)";
    public final String DELETE_PODERES_BY_NOMBRE_PODER = "DELETE FROM poderes WHERE nombre_poder = ?";
    public final String UPDATE_DANO_BY_NOMBRE_PODER = "UPDATE poderes SET dano = ? WHERE nombre_poder = ?";
    public final String SELECT_AVG_DANO = "SELECT avg(dano)as dano FROM poderes";

    Connection con;

    public PoderesJDBC(Connection con){


        this.con = con;
    }
    @Override
    public ArrayList<Poderes> select_all() {
        ArrayList<Poderes> lista_poderes = new ArrayList<>();
        try {
            PreparedStatement ptm = con.prepareStatement(SELECT_ALL);
            ResultSet rs = ptm.executeQuery();

            while (rs.next()){
                 String nombre_poderes= rs.getString("nombre_poder");
                 String tipo = rs.getString("tipo");
                 int dano = rs.getInt("dano");

                 Poderes pi = new Poderes(nombre_poderes, tipo, dano);
                 lista_poderes.add(pi);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_poderes;
    }

    @Override
    public Poderes select_nombre_poder( String nombre_poder) {
        Poderes ui = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_NOMBRE_PODER);
           stmt.setString(1,nombre_poder);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                String nombre_poder1 = rs.getString("nombre_poder");
                String tipo = rs.getString("tipo");
                int dano = rs.getInt("dano");
                ui = new Poderes(nombre_poder1 ,tipo,dano);
            }else {
                ui = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return ui;
    }
    @Override
    public Poderes select_tipo(String tipo) {
        Poderes ui = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_TIPO);
            stmt.setString(1,tipo);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                String nombre_poder = rs.getString("nombre_poder");
                String tipo1 = rs.getString("tipo");
                int dano = rs.getInt("dano");
                ui = new Poderes(nombre_poder ,tipo1,dano);
            }else {
                ui = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return ui;
    }

    @Override
    public Poderes select_dano(int dano) {
        Poderes dañoo = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_DANO);
            stmt.setInt(1,dano);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                String nombre_poder = rs.getString("nombre_poder");
                String tipo = rs.getString("tipo");
                int daño1 = rs.getInt("dano");
                dañoo = new Poderes(nombre_poder ,tipo,daño1);
            }else {
                dañoo = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dañoo;
    }
    @Override
    public boolean insert_poderes(Poderes poderes) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PODERES);
            stmt.setString(1, poderes.getNombre_poderes());
            stmt.setString(2, poderes.getTipo());
            stmt.setInt(3, poderes.getDaño());
            int x = stmt.executeUpdate();
            if(x!=1){
                salida = false;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
    @Override
    public boolean delete_poderes_by_nombre_poder(String nombre_poder) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PODERES_BY_NOMBRE_PODER);
            stmt.setString(1, nombre_poder );
            int x = stmt.executeUpdate();
            if(x!=1){
                salida = false;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
    @Override
    public boolean update_dano_by_nombre_poder( String nombre_poder ,int dano) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_DANO_BY_NOMBRE_PODER);
            stmt.setInt(1, dano);
            stmt.setNString(2, nombre_poder);
            int x = stmt.executeUpdate();
            if(x!=1){
                salida = false;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
    @Override
    public int select_avg_dano() {
        int danoo=0 ;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_AVG_DANO);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                danoo = rs.getInt("dano");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return danoo;
    }
}
