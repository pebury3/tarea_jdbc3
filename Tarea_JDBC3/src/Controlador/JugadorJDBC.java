package Controlador;

import Modulo.Jugador;
import Modulo.JugadorDAO;
import Modulo.Profesion;

import java.sql.*;
import java.util.ArrayList;

public class JugadorJDBC implements JugadorDAO {

    public final String SELECT_ALL0 = "SELECT * FROM jugador";

    public final String SELECT_BY_ID = "SELECT * FROM jugador WHERE id = ?";
    public final String SELECT_BY_NAME= "SELECT * FROM jugador WHERE name = ?";
    public final String SELECT_BY_NICKNAME= "SELECT * FROM jugador WHERE nickname = ?";
    public final String SELECT_BY_EMAIL = "SELECT * FROM jugador WHERE email = ?";
    public final String SELECT_BY_CLAVE= "SELECT * FROM jugador WHERE clave = ?";
    public final String UPDATE_JUGADOR = "UPDATE jugador SET id = ?, name =?, email =?, clave = ?, nickname = ? WHERE id = ?";
    public final String UPDATE_NICKNAME_BY_ID = "UPDATE jugador SET nickname = ? WHERE id = ?";
    public final String DELETE_JUGADOR_BY_ID = "DELETE FROM jugador WHERE id = ?";
    public final String INSERT_JUGADOR = "INSERT jugador VALUES(?, ?, ?, ?, ?)";
    Connection con;
    public JugadorJDBC(Connection con){

        this.con = con;
    }
    @Override
    public ArrayList<Jugador> select_all0() {
        ArrayList<Jugador> lista_jugadores = new ArrayList<Jugador>();
        try {
            PreparedStatement pmt = con.prepareStatement(SELECT_ALL0);
            ResultSet rs = pmt.executeQuery();

            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                int clave = rs.getInt("clave");
                String nickname = rs.getString("nickname");

                Jugador ui = new Jugador(id, name, email, clave, nickname);
                lista_jugadores.add(ui);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_jugadores;
    }

    @Override
    public Jugador select_by_id(int id) {
        Jugador ui = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                int id2 = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                int clave = rs.getInt("clave");
                String nickname = rs.getString("nickname");

                ui = new Jugador(id2,name, email, clave, nickname);

            }else {
                ui = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ui;
    }

    @Override
    public Jugador select_by_nickname(String nickname) {
        Jugador ui = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_NICKNAME);
            stmt.setString(1,nickname);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                int clave = rs.getInt("clave");
                String nickname1 = rs.getString("nickname");

                ui = new Jugador(id,name, email, clave, nickname1);

            }else {
                ui = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ui;
    }

    @Override
    public Jugador select_by_name(String name) {
        Jugador ui = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_NAME);
            stmt.setString(1,name);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                int id = rs.getInt("id");
                String name2 = rs.getString("name");
                String email = rs.getString("email");
                int clave = rs.getInt("clave");
                String nickname = rs.getString("nickname");

                ui = new Jugador(id,name2, email, clave, nickname);

            }else {
                ui = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ui;
    }
    @Override
    public Jugador select_email(String email) {
        Jugador ui = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_EMAIL);
            stmt.setString(1,email);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email1 = rs.getString("email");
                int clave = rs.getInt("clave");
                String nickname = rs.getString("nickname");

                ui = new Jugador(id,name, email1, clave, nickname);

            }else {
                ui = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ui;
    }

    @Override
    public Jugador select_clave(int clave) {
        Jugador ui = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_CLAVE);
            stmt.setInt(1,clave);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                int clave1 = rs.getInt("clave");
                String nickname = rs.getString("nickname");

                ui = new Jugador(id,name, email, clave1, nickname);

            }else {
                ui = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ui;
    }

    @Override
    public boolean insert_jugador(Jugador jugador) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_JUGADOR);
            stmt.setInt(1, jugador.getId());
            stmt.setString(2, jugador.getName());
            stmt.setString(3, jugador.getEmail());
            stmt.setInt(4,jugador.getClave());
            stmt.setNString(5, jugador.getNickname());


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }


    @Override
    public boolean update_jugador(int id,int id2,String name, String email, int clave, String nickname) {
        boolean salida = true;
        try {
            /*  jugador SET id = ?, name =?, email =? clave = ?, nickname = ? WHERE ID = ? */
            PreparedStatement stmt = con.prepareStatement(UPDATE_JUGADOR);
            stmt.setInt(1, id2);
            stmt.setString(2,name);
            stmt.setString(3,email);
            stmt.setInt(4, clave);
            stmt.setString(5,nickname);
            stmt.setInt(6, id);



            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }

    @Override
    public boolean update_nickname(int id ,String nickname) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_NICKNAME_BY_ID);
            stmt.setString(1, nickname);
            stmt.setInt(2, id);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }

    @Override
    public boolean delete_jugador_by_id(int id) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_JUGADOR_BY_ID);
            stmt.setInt(1, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }

}
