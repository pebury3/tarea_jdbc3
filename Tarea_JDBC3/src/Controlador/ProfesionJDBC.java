package Controlador;

import Modulo.Poderes;
import Modulo.Profesion;
import Modulo.ProfesionDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProfesionJDBC implements ProfesionDAO {
    public final String SELECT_ALL = "SELECT * FROM profesion";
    public final String SELECT_PROFESION = "SELECT * FROM profesion WHERE tipo_profesion= ? ";
    public final String SELECT_ELEMENTO= "SELECT * FROM profesion WHERE elemento =? ";
    public final String SELECT_VIDA = "SELECT * FROM profesion WHERE vida =?";
    public final String SELECT_AVG_VIDA = "SELECT avg(vida) as vida FROM profesion";
    public final String INSERT_PROFESION = "INSERT profesion VALUES(?, ?, ?)";
    public final String DELETE_PROFESION_BY_TIPO_PROFESION = "DELETE FROM profesion WHERE tipo_profesion = ?";
    public final String UPDATE_VIDA_BY_TIPO_PROFESION = "UPDATE profesion SET vida = ? WHERE tipo_profesion = ?";
    /*
    public final String SELECT_ALL = "SELECT * FROM Tuser";
    public final String SELECT_BY_ID = "SELECT * FROM Tuser WHERE id = ?";
    public final String SELECT_AVG_SALARY = "SELECT avg(salary) FROM Tuser";
    public final String INSERT_USER = "INSERT Tuser VALUES(?, ?, ?, ?)";
    public final String DELETE_USER_BY_ID = "DELETE FROM Tuser WHERE id = ?";
    public final String UPDATE_SALARY_BY_ID = "UPDATE Tuser SET salary = ? WHERE id = ?";
     */

    Connection con;
    public ProfesionJDBC(Connection con){

        this.con = con;
    }

    @Override
    public ArrayList<Profesion> select_all2() {
        ArrayList <Profesion> lista_profesiones = new ArrayList<>();

        try {
            PreparedStatement ptt =con.prepareStatement(SELECT_ALL);
            ResultSet rs = ptt.executeQuery();
            while (rs.next()){
                 String tipo_profesion= rs.getString("tipo_profesion");
                 String elemento = rs.getString("elemento");
                 int vida = rs.getInt("vida");

                 Profesion pi =new Profesion(tipo_profesion, elemento,vida);
                 lista_profesiones.add (pi);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_profesiones;
    }

    @Override
    public Profesion select_tipo_profesion(String tipo_profesion) {
        Profesion tipo_pro = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_PROFESION);
            stmt.setString(1, tipo_profesion);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String tipo_profesion1 = rs.getString("tipo_profesion");
                String elementoo = rs.getString("elemento");
                int vida = rs.getInt("vida");
                tipo_pro = new Profesion(tipo_profesion1, elementoo, vida);
            } else {
                tipo_pro = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return tipo_pro;
    }
    @Override
    public Profesion select_elemento(String elemento){
            Profesion elemento1 = null;
            try {
                PreparedStatement stmt = con.prepareStatement(SELECT_ELEMENTO);
                stmt.setString(1, elemento);
                ResultSet rs = stmt.executeQuery();

                if (rs.next()) {
                    String tipo_profesion1 = rs.getString("tipo_profesion");
                    String elementoo = rs.getString("elemento");
                    int vida = rs.getInt("vida");
                    elemento1 = new Profesion(tipo_profesion1, elementoo, vida);
                } else {
                    elemento1 = null;
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            return elemento1;
        }
    @Override
    public Profesion select_vida(int vida) {
        Profesion vidaa = null;
                try {
                    PreparedStatement stmt = con.prepareStatement(SELECT_VIDA);
                    stmt.setInt(1,vida);
                    ResultSet rs = stmt.executeQuery();

                    if (rs.next()){
                        String tipo_profesion1 = rs.getString("tipo_profesion");
                        String elementoo = rs.getString("elemento");
                        int vida1 = rs.getInt("vida");
                        vidaa = new Profesion(tipo_profesion1, elementoo, vida1);
                    }else {
                        vidaa = null;
                    }
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                return vidaa;
    }

    @Override
    public int select_avg_vida() {
        int vidaav = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_AVG_VIDA);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                vidaav = rs.getInt("vida");
            }
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
            throw new RuntimeException(e);
        }
        return vidaav;
    }

    @Override
    public boolean insert_profesion(Profesion profesion) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PROFESION);
            stmt.setString(1, profesion.getTipo_profesion());
            stmt.setString(2, profesion.getElemento());
            stmt.setInt(3, profesion.getVida());

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean delete_profesion_by_tipo_profesion(String tipo_profesion) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PROFESION_BY_TIPO_PROFESION);
            stmt.setString(1, tipo_profesion );

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_vida_by_tipo_profesion(String tipo_profesion, int vida) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_VIDA_BY_TIPO_PROFESION);
            stmt.setString(1, tipo_profesion);
            stmt.setInt(2, vida);



            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }
}
