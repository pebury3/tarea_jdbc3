package Vista;

import Controlador.JugadorJDBC;
import Controlador.PoderesJDBC;
import Controlador.ProfesionJDBC;
import Modulo.Jugador;
import Modulo.Poderes;
import Modulo.Profesion;
import Utilidades.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean bucle = true;
        Connection conn = null;
        Conexion con = new Conexion();
        PreparedStatement stmt = null;

        while (bucle) {

            System.out.println("***********************************************************************");
            System.out.println("*                    *************************                        *");
            System.out.println("*                    *    JDBC  PAULSITO     *                        *");
            System.out.println("*                    *************************                        *");
            System.out.println("*                  * Escoge una opcion del menu *                     *");
            System.out.println("***********************************************************************");
            System.out.println("*  *SECCION JUGADORES:  *             * PRESS: 1 *                    *");
            System.out.println("***********************************************************************");
            System.out.println("*  *SECCION PODERES:    *             * PRESS: 2 *                    *");
            System.out.println("***********************************************************************");
            System.out.println("*  *SECCION PROFESION:  *             * PRESS: 3 *                    *");
            System.out.println("***********************************************************************");
            System.out.println("*  *SALIR:              *             * PRESS: 4 *                    *");
            System.out.println("***********************************************************************");
            int opcion;
            while (true) {
                if (sc.hasNextInt()) {
                    opcion = sc.nextInt();
                    break;
                } else {
                    System.out.println("Por favor, ingrese un número entero válido .");
                    sc.next(); // Limpiar el buffer del Scanner
                }
            }
            if (opcion == opcion) {
                switch (opcion) {

                    case 1:
                        boolean bucle2 = true;
                        while (bucle2) {
                            System.out.println("***********************************************************************");
                            System.out.println("*                    *************************                        *");
                            System.out.println("*                    *    JDBC  PAULSITO     *                        *");
                            System.out.println("*                    *************************                        *");
                            System.out.println("*                  * Escoge una opcion del menu *                     *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Mostrar todos jugadores:         *             * PRESS: 1  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Mostrar  datos por ID:           *             * PRESS: 2  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Mostrar  datos por NAME:         *             * PRESS: 3  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Mostrar  datos por EMAIL:        *             * PRESS: 4  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Mostrar  datos por NICKNAME:     *             * PRESS: 5  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Mostrar  datos por CLAVE:        *             * PRESS: 6  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Agrega un USUARIO:               *             * PRESS: 7  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Actualiza una CLAVE por ID:      *             * PRESS: 8  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Actualiza un NICKNAME por ID:    *             * PRESS: 9  *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *Elimina un JUGADOR por ID:       *             * PRESS: 10 *      *");
                            System.out.println("***********************************************************************");
                            System.out.println("*  *VOLVER AL MENU ANTERIOR:         *             * PRESS:  0 *      *");
                            System.out.println("***********************************************************************");


                            int opcion2;
                            while (true) {
                                if (sc.hasNextInt()) {
                                    opcion2 = sc.nextInt();
                                    break;
                                } else {
                                    System.out.println("Por favor, ingrese un número entero válido ");
                                    sc.next(); // Limpiar el buffer del Scanner
                                }
                            }
                            if (opcion2 == opcion2) {
                                switch (opcion2) {

                                    case 1:
                                        try {

                                            //paso 3: Open a connection
                                            System.out.println("Connecting to database...");
                                            System.out.println("mostrando lista completa");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            ArrayList<Jugador> lista = controller.select_all0();
                                            if (lista != null) {
                                                for (Jugador u : lista) {
                                                    System.out.println("La lista es: " + u);
                                                }
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }
                                        break;
                                    case 2:

                                        System.out.println("escribe un ID");
                                        int ids;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                ids = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido para el ID del jugador.");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }

                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Jugador ui = controller.select_by_id(ids);

                                            if (ui != null) {

                                                System.out.println("La lista por ID es:" + ui);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();

                                        }
                                        break;
                                    case 3:
                                        System.out.println("escribe un nombre");
                                        String names = sc.next();

                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Jugador name1 = controller.select_by_name(names);

                                            if (name1 != null) {
                                                System.out.println("La lista por NOMBRE es:" + name1);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }

                                        break;
                                    case 4:
                                        System.out.println("escribe un CORREO");
                                        String email = sc.next();

                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Jugador emails = controller.select_email(email);

                                            if (emails != null) {
                                                System.out.println("La lista por EMAIL es:" + emails);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }
                                        break;
                                    case 5:
                                        System.out.println("escribe un NICKNAME");
                                        String nicknammee = sc.next();

                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Jugador ui = controller.select_by_nickname(nicknammee);

                                            if (ui != null) {
                                                System.out.println("La lista por NICKNAME es:" + ui);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }

                                        break;
                                    case 6:
                                        System.out.println("escribe una CLAVE");
                                        int clavee;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                clavee = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido ");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }

                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Jugador ui = controller.select_clave(clavee);

                                            if (ui != null) {
                                                System.out.println("La lista por CLAVE es:" + ui);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }

                                        break;

                                    case 7:
                                        System.out.println("Para crear un jugador debes poner los siguientes datos");
                                        System.out.println("   ");
                                        System.out.println("id");
                                        int idsss;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                idsss = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        System.out.println("nombre");
                                        String namess = sc.next();
                                        System.out.println("email");
                                        String emailss = sc.next();
                                        System.out.println("clave");
                                        int clavess;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                clavess = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido ");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        System.out.println("nickname");
                                        String nicknamess = sc.next();


                                        Jugador jugador22 = new Jugador(idsss, namess, emailss, clavess, nicknamess);

                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Boolean ui = controller.insert_jugador(jugador22);
                                            if (ui != null) {

                                                System.out.println("se ha creado " + jugador22);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }

                                        break;
                                    case 8:
                                        System.out.println("ID del JUGADOR que quieres modificar");
                                        int idss;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                idss = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        System.out.println("ID NUEVO");
                                        int idsnv;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                idsnv = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        System.out.println("NOMBRE nuevo ");
                                        String nombrenv = sc.next();
                                        System.out.println("EMAIL nuevo ");
                                        String emailnv = sc.next();
                                        System.out.println("clave");
                                        int clavss;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                clavss = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        System.out.println("NICKNAME nuevo ");
                                        String nicknamenv = sc.next();
                                        /*  jugador SET id = ?, name =?, email =? clave = ?, nickname = ? WHERE ID = ? */
                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Boolean ui = controller.update_jugador(idss, idsnv, nombrenv, emailnv, clavss, nicknamenv);


                                            if (ui != null) {
                                                System.out.println("Se han cambiado los DATOS del ID:  " + idss);
                                                System.out.println("los datos NUEVOS son: " + clavss);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }

                                        break;
                                    case 9:
                                        System.out.println("ID del NICKNAME que quieres modificar");
                                        int id;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                id = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido.");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        System.out.println("nickname");
                                        String nicknam = sc.next();

                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Boolean ui = controller.update_nickname(id, nicknam);

                                            if (ui != null) {

                                                System.out.println("Se ha cambiado el NICKNAME del ID:  " + id);
                                                System.out.println("el NUEVO NICKNAME ES: " + nicknam);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }
                                        break;
                                    case 10:
                                        System.out.println("ID QUE QUIERES BORRAR");
                                        int iddl;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                iddl = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido ");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            JugadorJDBC controller = new JugadorJDBC(conn);
                                            Boolean ui = controller.delete_jugador_by_id(iddl);

                                            if (ui != null) {

                                                System.out.println("se ha borrado :" + iddl);
                                                System.out.println("ID Y datos BOORADOS:  " + ui);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }
                                        break;
                                    case 0:
                                        bucle2= false;
                                        System.out.println("volviendo al menu anterior");

                                }
                            }
                        }break;
                    case 2:
                        boolean bucle3 = true;
                        while (bucle3) {
                        System.out.println("***********************************************************************");
                        System.out.println("*                    *************************                        *");
                        System.out.println("*                    *    JDBC  PAULSITO     *                        *");
                        System.out.println("*                    *************************                        *");
                        System.out.println("*                  * Escoge una opcion del menu *                     *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar todos PODERES:                 *       * PRESS: 1  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar datos por NOMBRE DE PODER:     *       * PRESS: 2  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar  datos por TIPO DE PODER:      *       * PRESS: 3  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar  datos por DAÑO:               *       * PRESS: 4  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Insertar PODER:                        *       * PRESS: 5  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Actualizar DAÑO por NOMBRE DE PODER:   *       * PRESS: 6  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Borrar PODER por NOMBRE DE PODER :     *       * PRESS: 7  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *AVG de DAÑO:                          *        * PRESS: 8  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *VOLVER AL MENU ANTERIOR:         *             * PRESS:  0 *      *");
                        System.out.println("***********************************************************************");

                        int opcion3;
                            while (true) {
                                if (sc.hasNextInt()) {
                                    opcion3 = sc.nextInt();
                                    break;
                                } else {
                                    System.out.println("Por favor, ingrese un número entero válido ");
                                    sc.next(); // Limpiar el buffer del Scanner
                                }
                            }
                        if (opcion3 == opcion3) {
                            switch (opcion3) {
                                case 1:
                                    try {
                                        //paso 3: Open a connection
                                        System.out.println("Connecting to database...");
                                        conn = con.getConnection();
                                        PoderesJDBC controller = new PoderesJDBC(conn);
                                        ArrayList<Poderes> lista = controller.select_all();
                                        if (lista != null) {
                                            for (Poderes u : lista) {

                                                System.out.println(u);
                                            }
                                        } else {
                                            System.out.println("la lista no existe");
                                        }
                                    } catch (Exception e) {
                                        //Handle errors for Class.forName
                                        e.printStackTrace();
                                    }
                                    break;
                                case 2:
                                    System.out.println("nombre del poder");
                                    String names = sc.next();

                                    try {
                                        System.out.println("Connecting to database...");
                                        conn = con.getConnection();
                                        PoderesJDBC controller = new PoderesJDBC(conn);
                                        Poderes name1 = controller.select_nombre_poder(names);

                                        if (name1 != null) {
                                            System.out.println("La lista por NOMBRE_PODER es:" + names + " " + name1);
                                        } else {
                                            System.out.println("la lista no existe");
                                        }
                                    } catch (Exception e) {
                                        //Handle errors for Class.forName
                                        e.printStackTrace();
                                    }
                                    break;
                                case 3:
                                    System.out.println("tipo de poder");
                                    String tiposs = sc.next();

                                    try {
                                        System.out.println("Connecting to database...");
                                        conn = con.getConnection();
                                        PoderesJDBC controller = new PoderesJDBC(conn);
                                        Poderes name1 = controller.select_tipo(tiposs);

                                        if (name1 != null) {
                                            System.out.println("La lista por TIPO es:" + tiposs + " " + name1);
                                        } else {
                                            System.out.println("la lista no existe");
                                        }
                                    } catch (Exception e) {
                                        //Handle errors for Class.forName
                                        e.printStackTrace();
                                    }
                                    break;
                                case 4:
                                    System.out.println("daño a buscar");
                                    int dañoos;
                                    while (true) {
                                        if (sc.hasNextInt()) {
                                            dañoos = sc.nextInt();
                                            break;
                                        } else {
                                            System.out.println("Por favor, ingrese un número entero válido ");
                                            sc.next(); // Limpiar el buffer del Scanner
                                        }
                                    }
                                    try {
                                        System.out.println("Connecting to database...");
                                        conn = con.getConnection();
                                        PoderesJDBC controller = new PoderesJDBC(conn);
                                        Poderes name1 = controller.select_dano(dañoos);
                                        if (name1 != null) {
                                            System.out.println("La lista por TIPO es:" + dañoos + " " + name1);
                                        } else {
                                            System.out.println("la lista no existe");
                                        }
                                    } catch (Exception e) {
                                        //Handle errors for Class.forName
                                        e.printStackTrace();
                                    }
                                    break;
                                case 5:
                                    System.out.println("debes saber la clave para ingresar poderes");
                                    int clavepo = 55555;
                                    int claveus;
                                    while (true) {
                                        if (sc.hasNextInt()) {
                                            claveus = sc.nextInt();
                                            break;
                                        } else {
                                            System.out.println("Por favor, ingrese un número entero válido ");
                                            sc.next(); // Limpiar el buffer del Scanner
                                        }
                                    }
                                    if (clavepo == claveus) {
                                        System.out.println("CLAVE CORRECTA");
                                        System.out.println(" ");
                                        System.out.println(" ");
                                        System.out.println("nombre del poder");
                                        String nombrpd = sc.next();
                                        System.out.println("tipo");
                                        String tipoo = sc.next();
                                        System.out.println("daño");
                                        int dañodñ;
                                        while (true) {
                                            if (sc.hasNextInt()) {
                                                dañodñ = sc.nextInt();
                                                break;
                                            } else {
                                                System.out.println("Por favor, ingrese un número entero válido ");
                                                sc.next(); // Limpiar el buffer del Scanner
                                            }
                                        }
                                        Poderes poderes = new Poderes(nombrpd, tipoo, dañodñ);
                                        try {
                                            System.out.println("Connecting to database...");
                                            conn = con.getConnection();
                                            PoderesJDBC controller = new PoderesJDBC(conn);
                                            Boolean ui = controller.insert_poderes(poderes);

                                            if (ui != null) {

                                                System.out.println("se ha creado " + poderes);
                                            } else {
                                                System.out.println("la lista no existe");
                                            }
                                        } catch (Exception e) {
                                            //Handle errors for Class.forName
                                            e.printStackTrace();
                                        }
                                    } else {
                                        System.out.println("clave incorrecta");
                                        System.out.println("");
                                        System.out.println("");
                                    }
                                    break;
                                case 6:
                                    System.out.println("Nombre del poder");
                                    String nombreup = sc.next();
                                    System.out.println("Daño");
                                    int dañoup;
                                    while (true) {
                                        if (sc.hasNextInt()) {
                                            dañoup = sc.nextInt();
                                            break;
                                        } else {
                                            System.out.println("Por favor, ingrese un número entero válido ");
                                            sc.next(); // Limpiar el buffer del Scanner
                                        }
                                    }
                                    try {
                                        System.out.println("Connecting to database...");
                                        conn = con.getConnection();
                                        PoderesJDBC controller = new PoderesJDBC(conn);
                                        Boolean ui = controller.update_dano_by_nombre_poder(nombreup, dañoup);
                                        if (ui != null) {
                                            System.out.println("Se ha Actualizo el DAÑO del PODER" + nombreup);
                                            System.out.println("El nuevo DAÑO es: " + dañoup);
                                        } else {
                                            System.out.println("la lista no existe");
                                        }
                                    } catch (Exception e) {
                                        //Handle errors for Class.forName
                                        e.printStackTrace();
                                    }
                                    break;
                                case 7:
                                    System.out.println("nombre del poder que quieres borrar");
                                    String nombredl = sc.next();
                                    try {
                                        System.out.println("Connecting to database...");
                                        conn = con.getConnection();
                                        PoderesJDBC controller = new PoderesJDBC(conn);
                                        Boolean ui = controller.delete_poderes_by_nombre_poder(nombredl);
                                        if (ui != null) {

                                            System.out.println("se ha borrado :" + nombredl);
                                        } else {
                                            System.out.println("la lista no existe");
                                        }
                                    } catch (Exception e) {
                                        //Handle errors for Class.forName
                                        e.printStackTrace();
                                    }
                                    break;
                                case 8:
                                    try {
                                        System.out.println("Connecting to database...");
                                        conn = con.getConnection();
                                        PoderesJDBC controller = new PoderesJDBC(conn);
                                        int ui = controller.select_avg_dano();
                                        System.out.println(ui);
                                        System.out.println("la lista no existe");

                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                    break;
                                case 0:
                                    bucle3= false;
                                    System.out.println("volviendo al menu anterior");

                            }
                        }
                        }break;
                    case 3:
                        boolean bucle4 = true;
                        while (bucle4) {
                        System.out.println("***********************************************************************");
                        System.out.println("*                    *************************                        *");
                        System.out.println("*                    *    JDBC  PAULSITO     *                        *");
                        System.out.println("*                    *************************                        *");
                        System.out.println("*                  * Escoge una opcion del menu *                     *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar todas  las PROFESIONES:           *    * PRESS: 1  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar datos por TIPO DE  PROFESION:     *    * PRESS: 2  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar  datos por ELEMENTO:              *    * PRESS: 3  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Mostrar  datos por VIDA:                  *    * PRESS: 4  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *AVG de VIDA:                              *    * PRESS: 5  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Insertar PROFESION:                       *    * PRESS: 6  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Borrar PROFESION por TIPO DE PROFESION :  *    * PRESS: 7  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *Actualizar VIDA por TIPO DE PROFESION:    *    * PRESS: 6  *      *");
                        System.out.println("***********************************************************************");
                        System.out.println("*  *VOLVER AL MENU ANTERIOR:         *             * PRESS:  0 *      *");
                        System.out.println("***********************************************************************");

                        int opcion4;
                            while (true) {
                                if (sc.hasNextInt()) {
                                    opcion4 = sc.nextInt();
                                    break;
                                } else {
                                    System.out.println("Por favor, ingrese un número entero válido ");
                                    sc.next(); // Limpiar el buffer del Scanner
                                }
                            }
                            if (opcion4 == opcion4) {
                        switch (opcion4) {

                            case 1:
                                try {
                                    //paso 3: Open a connection
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    ArrayList<Profesion> lista = controller.select_all2();
                                    if (lista != null) {
                                        for (Profesion u : lista) {

                                            System.out.println(u);
                                        }
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }
                                break;
                            case 2:
                                System.out.println("TIPO de la PROFESION");
                                String names = sc.next();

                                try {
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    Profesion name1 = controller.select_tipo_profesion(names);

                                    if (name1 != null) {
                                        System.out.println("La lista por TIPO de PROFESION es:" + names + " " + name1);
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }
                                break;
                            case 3:
                                System.out.println("nombre de la PROFESION");
                                String nameel = sc.next();

                                try {
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    Profesion name1 = controller.select_elemento(nameel);

                                    if (name1 != null) {
                                        System.out.println("La lista por ELEMENTO es:" + nameel + " " + name1);
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }

                                break;
                            case 4:
                                System.out.println("por vida VIDA ");
                                int vidae;
                                while (true) {
                                    if (sc.hasNextInt()) {
                                        vidae = sc.nextInt();
                                        break;
                                    } else {
                                        System.out.println("Por favor, ingrese un número entero válido ");
                                        sc.next(); // Limpiar el buffer del Scanner
                                    }
                                }
                                try {
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    Profesion name1 = controller.select_vida(vidae);

                                    if (name1 != null) {
                                        System.out.println("La lista por VIDA es:" + vidae + " " + name1);
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }

                                break;
                            case 5:

                                try {
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    int name1 = controller.select_avg_vida();

                                    if (name1 != 0) {
                                        System.out.println("El AVG de la VIDA es:" + " " + name1);
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }
                                break;
                            case 6:
                                System.out.println("tipo de profesion");
                                String tipodeprofe = sc.next();
                                System.out.println("elemento");
                                String elemento = sc.next();
                                System.out.println("vida");
                                int vidain;
                                while (true) {
                                    if (sc.hasNextInt()) {
                                        vidain = sc.nextInt();
                                        break;
                                    } else {
                                        System.out.println("Por favor, ingrese un número entero válido ");
                                        sc.next(); // Limpiar el buffer del Scanner
                                    }
                                }
                                Profesion profesionin = new Profesion(tipodeprofe, elemento, vidain);

                                try {
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    Boolean ui = controller.insert_profesion(profesionin);

                                    if (ui != null) {

                                        System.out.println("se ha creado:  " + profesionin);
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }
                                break;
                            case 7:
                                System.out.println("tipo de Profesion que quieres borrar");
                                String tipodl = sc.next();
                                try {
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    Boolean ui = controller.delete_profesion_by_tipo_profesion(tipodl);
                                    if (ui != null) {

                                        System.out.println("se ha borrado :" + tipodl);
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }
                                break;
                            case 8:
                                System.out.println("tipo de PROFESION");
                                String tipoup = sc.next();
                                System.out.println("vida");
                                int vidaup;
                                while (true) {
                                    if (sc.hasNextInt()) {
                                        vidaup = sc.nextInt();
                                        break;
                                    } else {
                                        System.out.println("Por favor, ingrese un número entero válido ");
                                        sc.next(); // Limpiar el buffer del Scanner
                                    }
                                }
                                try {
                                    System.out.println("Connecting to database...");
                                    conn = con.getConnection();
                                    ProfesionJDBC controller = new ProfesionJDBC(conn);
                                    Boolean ui = controller.update_vida_by_tipo_profesion(tipoup, vidaup);
                                    if (ui != null) {
                                        System.out.println("Se ha Actualizo el VIDA de la  PROFESION" + tipoup);
                                        System.out.println("La nueva VIDA es: " + vidaup);
                                    } else {
                                        System.out.println("la lista no existe");
                                    }
                                } catch (Exception e) {
                                    //Handle errors for Class.forName
                                    e.printStackTrace();
                                }
                                break;
                            case 0:
                                bucle4= false;
                                System.out.println("volviendo al menu anterior");
                        }
                            }
                        }break;
                    case 4:
                        bucle= false;
                        System.out.println("saliste de todo");
                }
            }
        }   System.out.println("Goodbye!");
        }
}