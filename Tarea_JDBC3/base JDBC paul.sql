create database Paulsito;
use Paulsito;

create table jugador(
	id int primary key,
    name varchar(255),
    email varchar(255) unique,
    clave int,
    nickname varchar (30) unique
    );
    
create table profesion(
	tipo_profesion varchar (255) primary key,
    elemento varchar (255),
    vida int
);        
    
create table poderes(
    nombre_poder varchar(255) primary key unique,
    tipo varchar (255),
    daño int (255) 
    );
    
    SELECT avg(vida) FROM profesion;
    alter table poderes rename column daño to dano;
    
insert jugador values (1, "Paul", "user1@mail.com", 123445678, "Proo1");
insert jugador values (2, "paloma", "user2@mail.com", 45679123, "PROfesorita");
insert jugador values (3, "harry", "user3@mail.com", 5000555, "Manquito");
insert jugador values (4, "luna", "user4@mail.com", 689725, "Mas Manquito");


insert profesion values ("druida", "Tierra  y Agua", 500);
insert profesion values ("Sorcerer", "Fuego y Muerte", 500);
insert profesion values ("Archero", "Holy", 800);
insert profesion values ("Guerrero", "Fisico", 1200);

insert poderes values ( "Chorro de AGUA", "Agua", 250);
insert poderes values ( "Tierra en tus ojos", "Tierra", 150);
insert poderes values ( "Bola de Fuego","Fuego", 250);
insert poderes values ( "Avada Kedavra", "Muerte", 300);
insert poderes values ("Golpe Santo","Holy",  260);
insert poderes values ("Fisico", "Espada Filosa", 140);
